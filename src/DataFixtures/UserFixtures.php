<?php

namespace App\DataFixtures;
use App\Entity\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

  public function __construct(UserPasswordEncoderInterface $encoder)
  {
    $this->encoder = $encoder;
  }

  public function load(ObjectManager $manager)
  {

    $user = new User();
    $user->setFirstName('Enzo')
    ->setLastName('Avagliano')
    ->setMail('enzo@blog.com')
    ->setUsername('EloxFire')
    ->setPass($this->encoder->encodePassword($user, 'enzopass'));

    $manager->persist($user);
    $manager->flush();
  }
}
